<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/index.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title>Generate QR Code</title>
</head>
<body style="background-color: #CCCDCF;">
    <div class="container-fluid text-center mx-auto">
        <div class="row logo-container">
            <div class="col text-center" style="display: flex; justify-content:center; align-items:center;">
                <H1 style="color:white;">Receiving Barcode Scanner</H1>
            </div>
        </div>
        <div class="row mt-5 justify-content-center">
                <!-- <div class="col-10 col-sm-8 col-md-7 col-lg-4">
                    <div class="box-container col-12" style="border-radius:10px; ">

                    </div>
                </div> -->
                <div class="col-sm-12 col-md-10 col-lg-8 mt-4 mt-lg-0">
                    <div class="box-container col-12" style="border-radius:10px; background-color: white; box-shadow: 3px 3px 10px #888888;">
                        
                    </div>
                </div>
                <!-- <div class="col-10 col-sm-8 col-md-7 col-lg-4 mt-4 mt-lg-0">
                    <div class="box-container col-12" style="border-radius:10px; ">

                    </div>
                </div>  -->
        </div>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
</html>