<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Scanning</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <script src="./html5-qrcode.min.js">  
    </script>
    <script src="https://kit.fontawesome.com/51460444e6.js" crossorigin="anonymous"></script>
</head>
<body onload="scanQR()">
    <div id="reader" width="600px">
    </div>
    <div class="text-center mt-5">
        <a href="./index.php">
            <i style="color:red; z-index: 1; position:absolute; top:0; left:0" class="fas fa-window-close fa-3x"></i>
        </a>
    </div>

    <div class="text-center mt-5">Result</div>
    <div id="resultArea" class="text-center mt-2"></div>
    

<script src="./js/main.js"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
</body>
</html>