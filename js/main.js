// console.log("Sdfsdf")
const fileinput = document.getElementById('qr-input-file');


        let testFunction = () => {
            fetch('./sample.php', {
                method: 'POST',
                mode: "same-origin",
                credentials: "same-origin",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "result": "test"
                })
                
            })
            .then(response => response.json())
            .then(result => console.log(result))
        }

        let scanQR = () =>
        {
            const html5QrCode = new Html5Qrcode("reader", { fps: 10, qrbox: 250 });
            const qrCodeSuccessCallback = (decodedText, decodedResult) => 
            {
                /* handle success */
                // console.log(decodedText)
                var result = decodedText.split(";").map(data => data)
                document.getElementById("resultArea").innerHTML = `Part Number: ${result[6]}`
                console.log(decodedResult)
                window.location.href = "result.php?result="+decodedText
                
                
                stopScan()
            };
            
            const config = { fps: 10, qrbox: { width: 250, height: 250 } };
            console.log(html5QrCode)

            // If you want to prefer back camera
            html5QrCode.start({ facingMode: "environment" }, config, qrCodeSuccessCallback);

            // document.getElementById("scanBtn").style.display = "none"
            // document.getElementById("stopScanBtn").style.display = "flex"

        }

        let createQRCode = () => 
        {
            setTimeout(()=>{
                var qrcode = new QRCode("qr-code", {
                    width: 250,
                    height: 230
                });

                function makeCode () {    
                qrcode.makeCode(document.getElementById('scannedValue').value);
                }

                if(document.getElementById('scannedValue').value !== ""){
                    makeCode();
                    // document.getElementById('scannedValue').value = ""
                } else {
                    document.getElementById("qr-code").style.display = "none"
                }
            },1000)
        }

        let stopScan = () =>
        {
            const html5QrCode = new Html5Qrcode("reader");
            html5QrCode.stop().then((ignore) => {
            // QR Code scanning is stopped.
            }).catch((err) => {
            // Stop failed, handle it.
            });
            document.getElementById("scanBtn").style.display = "flex"
            document.getElementById("stopScanBtn").style.display = "none"
        }

let search = ()=> {
    document.getElementById('dataTable').innerHTML = "";
    document.getElementById('table-data').style.display = "none"
    document.getElementById('spinnerBar').style.display = "block"
    if(document.getElementById('part_number').value !== ""){
        
        fetch('./test.php', {
        method: 'POST',
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.stringify({
            "data": document.getElementById('part_number').value
        })
        })

        .then(res => {
            
            return res.json()
        })
        .then(result => {
            // return console.log(result)
            document.getElementById('table-data').style.display = "block"
            document.getElementById('spinnerBar').style.display = "none"
            // console.log(result)
            // var table = document.getElementById('dataTable');
            var t = $('#data-table').DataTable();
            // console.log(result.length)
            for (let i = 0; i < result.length; i++) {
                // var row = table.insertRow(0);
                // var cell1 = row.insertCell(0);
                // var cell2 = row.insertCell(1);
                // var cell3 = row.insertCell(2);
                // var cell4 = row.insertCell(3);
                // var cell5 = row.insertCell(4);
                // var cell6 = row.insertCell(5);
                // var cell7 = row.insertCell(6);
                // cell1.innerHTML = result[i].ID;
                // cell2.innerHTML = result[i].GOODS_CODE;
                // cell3.innerHTML = result[i].SUPPLIER;
                // cell4.innerHTML = result[i].ASY_LINE;
                // cell5.innerHTML = result[i].ITEM_CODE;
                // cell6.innerHTML = result[i].PART_NUMBER;
                // cell7.innerHTML = result[i].PART_NAME;
                t.row.add([result[i].ID, result[i].GOODS_CODE, result[i].SUPPLIER, result[i].ASY_LINE, result[i].ITEM_CODE, result[i].PART_NUMBER, result[i].PART_NAME]).draw(false);
            }
            
        })
    }
            
}

$('#data-table tbody').on('click', 'tr', function () {
    var table = $('#data-table').DataTable();
    var data = table.row(this).data();
    alert('You clicked on ' + data[1] + "'s row");
});

if(document.getElementById('part_number')){
    document.getElementById('part_number').addEventListener('input', ()=>{
        search();
    })
}



let oldValue = window.pageYOffset;
window.addEventListener('scroll', function(e){

    // Get the new Value
    let newValue = window.pageYOffset;
    //Subtract the two and conclude
    if(oldValue > newValue){
        document.querySelector('.qr-click').style.bottom = "10px"
        // console.log("up")
    } else {
        document.querySelector('.qr-click').style.bottom = "-60px"
        // console.log("down")
    }

    // Update the old value
    oldValue = newValue;

});
        // File based scanning
        
        // fileinput.addEventListener('change', e => 
        // {   
        //     const html5QrCode = new Html5Qrcode("reader");  
        //     if (e.target.files.length == 0) 
        //     {
        //     // No file selected, ignore 
        //         return;
        //     }
        //     const imageFile = e.target.files[0];
        //     // Scan QR Code
        //     console.log(e.target.files[0])
        //    document.getElementById("qr-image").innerHTML = html5QrCode.scanFile(imageFile, true)
        //     .then(decodedText => 
        //     {
        //         // success, use decodedText
        //         console.log(decodedText);
        //         var result = decodedText.split(";").map(data => data)
        //         console.log(result)
        //         document.getElementById("resultArea").innerHTML = `Part Number: ${result[6]}`
        //         // window.location.href = "result.php?result="+decodedText
        //     })
        //     .catch(err => 
        //     {
        //         // failure, handle it.
        //         console.log(`Error scanning file. Reason: ${err}`)
        //         document.getElementById("resultArea").innerHTML = err
        //     });
        // });
        // dsfds

        let testFetch = ()=> {
            fetch('/test.php')
            .then(res => res.json())
            .then(result => {
                // console.log(result[7])
            })
        }

        let testFetchData = ()=> {
            fetch('/test.php', {
                method: "GET",
                body: JSON.stringify({"name":"sherwin"}),
                headers: "Content-type: application/json"
            })
            .then(res => res.json())
            .then(result => {
                // console.log(result[7])
            })
        }

function printQR() {
    var QrContents = document.getElementById("qr-code").innerHTML;
    var a = window.open('', '', 'height=auto, width=auto');
    a.document.write('<html>');
    a.document.write('<body>');
    a.document.write(`<center>${QrContents}</center>`);
    a.document.write('</body></html>');
    a.document.close();
    a.print();
}                  
                

// document.getElementById("data-table").addEventListener("click", ()=>{
//     alert("sdafsdf")
// })

// TEST
printFunction = ()=>{
    alert("test")
    window.print()
}

