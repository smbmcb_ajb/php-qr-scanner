<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QR Scanner</title>
</head>
<body>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Receiving QR Scanner</title>
    <link rel="stylesheet" href="assets/css/main/app.css">
    <link rel="stylesheet" href="assets/css/main/app-dark.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <link rel="stylesheet" href="assets/extensions/filepond/filepond.css">
    <link rel="stylesheet" href="assets/extensions/filepond-plugin-image-preview/filepond-plugin-image-preview.css">
    <link rel="stylesheet" href="assets/css/pages/filepond.css">
    <link rel="stylesheet" href="./css/index.css">
    <script src="https://kit.fontawesome.com/51460444e6.js" crossorigin="anonymous"></script>
    <script src="./html5-qrcode.min.js">  
    
    </script>
    
</head>
<body style="background-color: #CCCDCF;">
    
    <div class="container-fluid">
        <div class="row">
            <div style="background-color: #3C5393; height: 17rem; " class="col text-center">
                <img class="mt-3 matech-logo" src="./images/matech_logo_2.png" alt="logo" style="height: 200px; width: 300px; ">
                <H2 style="color: white;">M.A. TECHNOLOGY INC.</H2>
            </div>
        </div>
        <div class="row">
            <div class="col text-center mt-3">
                <p class="h1" style="color: #3C5393;">Receiving QR Scanner</p>
            </div>
        </div>
        <div class="row">
            <div class="col text-center mt-3">
                <!-- <a href="./scanner_page.php"><i class="fa-solid fa-qrcode fa-8x"></i></a> -->
                <a href="./scanner_page.php"><img src="./images/qr.png" alt="qr icon" style="width: 200px;height:200px"></a>
            </div>
        </div>
        <div class="row">
            <div class="col text-center mt-5">
                <img src="./images/new-file.png" alt="qr icon" style="width: 200px;height:200px">
            </div>
        </div>
        <!-- <div class="row">
            <div class="col text-center mt-3" id="reader" width="100px" height="100px">
                <input class="basic-filepond" id="qr-input-file" accept="image/*">
            </div>
            
        </div> -->
        
        <!-- <div class="text-center mt-5">Result</div>
        <div id="resultArea" class="text-center mt-2"></div>
         -->
            
    </div>

    
<!-- <script src="./main.js"></script> -->
<!-- // -->

<script src="assets/extensions/filepond/filepond.js"></script>
<script src="assets/js/pages/filepond.js"></script>

<script src="assets/js/bootstrap.js"></script>
<script src="./js/main.js"></script>
<!-- <script src="assets/js/app.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
</body>
</html>
</body>
</html>