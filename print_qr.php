<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QR System</title>
    <link rel="stylesheet" href="assets/css/main/app.css">
    <link rel="stylesheet" href="assets/css/main/app-dark.css">
    <link rel="stylesheet" href="./css/print_qr.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://kit.fontawesome.com/51460444e6.js" crossorigin="anonymous"></script>
</head>
<body style="background-color: #CCCDCF;">
<div class="container-fluid text-center mx-auto" >
    <div class="row mt-3 w-100 mx-auto" style="background-color: #CCCDCF; ">
        <div class="row mx-auto mb-3" >
            <div id="table-data" class="col-sm-12 mt-4 mb-3" style="overflow-x: auto; ">
                <table id="data-table" class="table table-bordered mb-3 mt-5" style="background-color:white;min-width: 600px;width:100%; box-shadow: 0px 3px 10px #888888;">
                    <thead>
                        <tr style="background-color: #3C5393; color:white;">
                            <th class="text-center">PO</th>
                            <th class="text-center">GOODS CODE</th>
                            <th class="text-center">ITEM CODE</th>
                            <th class="text-center">ASSY LINE</th>
                            <th class="text-center">DATE RECEIVE</th>
                            <th class="text-center">INVOICE</th>
                            <th class="text-center">QUANTITY</th>
                            <th class="text-center" style="word-break:normal;max-width: 50px;">TOTAL NO. OF BOX</th>
                            <th class="text-center" style="word-break:normal;max-width: 50px;">BOX LABEL QR</th>
                            <th class="text-center" style="word-break:normal;max-width: 50px;">INNER LABEL QR</th>
                            
                        </tr>
                    </thead>
                    
                    <!-- <tbody id="dataTable"></tbody> -->
                    <tfoot>
                        <tr><td colspan="10"><button class="btn btn-success " id="printQR" style="background-color: #3C5393;margin-left:auto; display:flex; justify-content:center; align-items: center;"><span class="fa-solid fa-print fa-xl" style="vertical-align: middle;"></span></button></td></tr>
                    </tfoot>
                </table>
                
                <center>
                    <div id="spinnerBar" class="spinner-border mt-5" style="width: 3rem; height: 3rem;" role="status">
                        <span class="visually-hidden">Loading...</span>
                        
                    </div>
                </center>
                <!-- <div class="pagination pagination-primary">
                    <div class="page-item disabled">
                        <div class="page-link" tabindex="-1">Previous</div>
                    </div>
                    <div class="page-item active">
                        <div class="page-link">1</div>
                    </div>
                    <div class="page-item">
                        <div class="page-link">2</div>
                    </div>
                    <div class="page-item">
                        <div class="page-link">3</div>
                    </div>
                    <div class="page-item">
                        <div class="page-link">Next</div>
                    </div> -->
                    
                </div>
            </div>
            
        </div>
        
        <!-- <div class="row"> -->
            <!-- <button class="btn btn-success mx-auto mb-3" id="printQR" style="background-color: #3C5393;">Print</button> -->
            <!-- <button class="btn btn-success w-25 mx-auto mt-3 mb-3" id="btnGetRows" style="background-color: #3C5393;">Cancel</button> -->
        <!-- </div> -->
        <!-- <div style="display: flex;" id="qrcode-for-print">  -->
    </div>
</div>
<!-- <div id="testContainer"></div> -->
<script>
    var aData = [];
    // console.log("sadfasd")
    let search = ()=> {
    // document.getElementById('dataTable').innerHTML = "";
    // document.getElementById('table-data').style.display = "none"
    // document.getElementById('spinnerBar').style.display = "block"
    // if(document.getElementById('part_number').value !== ""){
        $(document).ready(function () {
            fetch('./api/qr_details.php', {
            method: 'POST',
            headers: {
                'Content-Type':'application/json'
            },
            // body: JSON.stringify({
            //     "data": document.getElementById('part_number').value
            // })
            })

            .then(res => {
                
                return res.json()
            })
            .then(result => {
                checkDuplicate(result)
                // console.log(result[0].BOXLABEL_CHECKBOX)
                document.getElementById('table-data').style.display = "block"
                document.getElementById('spinnerBar').style.display = "none"
                var table = $('#data-table').DataTable({
                    "order": [[ 4, "desc" ]],
                    select: true,
                    data: result,
                    columns: [
                        { data: "PO" },
                        { data: "GOODS_CODE" },
                        { data: "ITEM_CODE" },
                        { data: "ASY_LINE" },
                        { data: "DATE_RECEIVE" },
                        { data: "INVOICE" },
                        { data: "QTY" },
                        { data: "NUMBER_OF_BOX" },
                        { data: "isPrinted",
                            defaultContent: 
                            `<div class='form-check'><input class='form-check-input boxLabelQR' type='checkbox' value='' name='boxLabelQR'></div>`,
                            orderable: false,
                            render: function(data){
                                // console.log(data)
                                if(data === 1){
                                    // console.log("tested")
                                    return "<span style='color: red;'>Printed</span>"
                                }
                            },
                        },
                        { "data": null,
                            defaultContent: 
                            `<div class='form-check'><input class='form-check-input innerLabelQR' type='checkbox' value='' name='innerLabelQR'></div>`,
                            orderable: false,
                        },
                        
                    ],
                    // columnDefs: [
                    //         {
                    //             "targets": -1,
                                
                    //             "data": null,
                    //             // "defaultContent": '<input class="check" type="checkbox" checked>'
                    //         },
                    //     ],
                });
                // console.log(table.columns())
                
    
                // console.log(table.rows().data())
                // $('#data-table tbody').on( 'click', 'input', function () {
                //         var data = table.rows( $(this).parents('tr') ).data();
                //         console.log("clicked on " + data[0]);
                        
                        
                //     } );
                
            })
        });
        // fetch('./api/qr_details.php', {
        // method: 'POST',
        // headers: {
        //     'Content-Type':'application/json'
        // },
        // // body: JSON.stringify({
        // //     "data": document.getElementById('part_number').value
        // // })
        // })

        // .then(res => {
            
        //     return res.json()
        // })
        // .then(result => {
        //     console.log(result)
        //     document.getElementById('table-data').style.display = "block"
        //     document.getElementById('spinnerBar').style.display = "none"
        //     // console.log(result)
        //     var table = document.getElementById('dataTable');
        //     // var t = $('#data-table').DataTable();
        //     // console.log(result.length)
        //     for (let i = 0; i < result.length; i++) {
        //         var row = table.insertRow(0);
        //         var cell1 = row.insertCell(0);
        //         var cell2 = row.insertCell(1);
        //         var cell3 = row.insertCell(2);
        //         var cell4 = row.insertCell(3);
        //         var cell5 = row.insertCell(4);
        //         var cell6 = row.insertCell(5);
        //         var cell7 = row.insertCell(6);
        //         cell1.innerHTML = result[i].PO;
        //         cell2.innerHTML = result[i].GOODS_CODE;
        //         cell3.innerHTML = result[i].ITEM_CODE;
        //         cell4.innerHTML = result[i].ASY_LINE;
        //         cell5.innerHTML = result[i].DATE_RECEIVE;
        //         cell6.innerHTML = result[i].INVOICE;
        //         cell7.innerHTML = result[i].QTY;
        //         console.log(result[i].GOODS_CODE)
                // t.row.add([result[i].PO, result[i].GOODS_CODE, result[i].ITEM_CODE, result[i].ASY_LINE, result[i].DATE_RECEIVE, result[i].INVOICE, result[i].QTY]).draw(false);
                
            // }
            
        // })
    }
            
// }
setTimeout(()=>{
    $(document).ready(function(){

        

    });
    
},1000)
let samplePrint = ()=>{
    // if(document.getElementById('boxLabelQR').checked == true){
        
        // console.log('checked')
//     // }
//     $("#boxLabelQR").click(function() {
//     var $row = $(this).closest("tr"),
//         $tds = $row.find("td:nth-child(2)");

//     $.each($tds, function() {
//         console.log($(this).text());
//     });
    
// });
}
// $('#data-table tbody').on('click', 'tr', function() {
//     var table = $('#data-table').DataTable();
//     var data = table.row(this).data();
//     alert('You clicked on ' + data[1] + "'s row");
// });
window.onload = search();
// $(document).ready(function () {
//     $('#data-table').DataTable();
// });
    setTimeout(()=>{
        $(document).ready(function(){
            // document.writeln("res")
            // $('.btnDemo').on('click', function(){
            //     if($(".btnDemo").is(":checked")){
            //         console.log('hjdfhjdshfjad')
            //         }
                
            // })
            var aData = [];
            $('#data-table tbody').on('click', 'input', function () {
                // console.log(document.getElementById("tblQRContainer").innerText)
                var index
                // var aData = [];
                var table = $('#data-table').DataTable();
                // var data = table.row(this).data();
                var data = table.row( $(this).parents('tr') ).data();
                
                // currentRow = $(this)
                // var data = table.row( $(this).parents('tr') ).data();
                let aDataStringified = JSON.stringify(data);
                if($(this).prop("checked") == true){
                    if($(this).prop("name") == "boxLabelQR"){
                        console.log(data)
                        // console.log($(this).prop("name"))
                        $('body').append(`<div style='display:none;' id='${data.ID}'></div>`)
                        qrc = new QRCode(document.getElementById(data.ID), data.PO+";"+data.GOODS_CODE+";"+data.ITEM_CODE+";"+data.INVOICE);
                        aData.push(data)
                    }
                    
                }else{
                    if($(this).prop("name") == "boxLabelQR"){
                        document.getElementById(data.ID).innerHTML = ""
                        aData.map((item)=>{
                        // console.log(item.ID)
                        if(data.ID == item.ID){
                            // console.log(item)
                            index = aData.findIndex(el => el.ID == item.ID)
                            aData.splice(index, 1);
                            // console.log(aData.indexOf(item.ID))
                        }
                    })
                    }
                    
                }
                console.log(aData)
                // console.log(index)

                // if(aData.length > 0){
                //     document.querySelector('.innerLabelQR').disabled = true
                // }else {
                //     document.querySelector('.innerLabelQR').disabled = false
                // }

            })
            // setTimeout(()=>{
                // var qrcode = new QRCode("qrcode-for-print", {
                //         width: 60,
                //         height: 50
                //         // correctLevel : QRCode.CorrectLevel.H
                //     });
                //     qrcode.makeCode("Test data");
                // },1000)

                // qrc = new QRCode(document.getElementById("qrcode-for-print"), "PO202111-00002; PA0-00124; 228494; 175871; PO202111-00002");
                // let makeQR = (po, goodsCode, itemCode, invoice)=>{
                    
                //     return new QRCode(document.getElementById("qrcode-for-print"), po+";"+goodsCode+";"+itemCode+";"+invoice);
                //     // return firstString + " " + secondString;
                // }
                // console.log(makeQR(1,2,3,4))
                
                // bgColor('01')

            $('#printQR').on('click', function(){
                return Swal.fire({
                title: 'Are you sure you want to print these QR(s)?',
                // text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3c5393',
                cancelButtonColor: '#d33',
                confirmButtonText: "Yes, I'm sure!"
                }).then((result) => {
                    console.log(result.isConfirmed)
                if (result.isConfirmed === false) {
                    return
                }
                // var QrContents = document.getElementById("printableDiv").innerHTML;
                var a = window.open('', '', 'height=auto, width=auto, margin=0');
                a.document.write('<html>');
                a.document.write('<head>');
                a.document.write('<link rel="stylesheet" href="css/print_design.css" />')
                
                a.document.write('</head>');
                a.document.write("<body>");
                // console.log(aData)
                for(i=0; i<aData.length; i++){
                    console.log(aData[i].DATE_RECEIVE)
                    // console.log(typeof(aData[i].DATE_RECEIVE.split("-")[1]))
                    let monthData = aData[i].DATE_RECEIVE.split("-")[1]
                    // bgColor(monthData)
                    // console.log(bgColor(monthData))
                    updateQR_printStatus(aData[i].ID)
                    // console.log(updateQR_printStatus(aData[i].ID).MESSAGE)
                    // document.getElementById("qrcode-for-print").innerHTML = ""
                    // qrc = new QRCode(document.getElementById("qrcode-for-print"), aData[i].PO+";"+aData[i].GOODS_CODE+";"+aData[i].ITEM_CODE+";"+aData[i].INVOICE);
                    console.log(aData)
                    // console.log(makeQR(aData[i].PO, aData[i].GOODS_CODE, aData[i].ITEM_CODE, aData[i].INVOICE))
                    // console.log(aData[i].NUMBER_OF_BOX)
                    for(x=0; x<aData[i].NUMBER_OF_BOX; x++){
                        a.document.write("<div style='display:inline-block;'>");
                        a.document.write("<div style='max-width:259px; display:flex; flex-direction:row; height: 110px; '>");
                            a.document.write(`<div id='first-row-to-print' style='width:130px;border:1px solid black; background-color:${bgColor(monthData).bgColor}; color:${bgColor(monthData).color}'>`);
                            console.log(bgColor().bgColor)
                                a.document.write("<div style='height: 28px;display:flex; flex-direction:row;'>")
                                    a.document.write("<div style='height: 28px;width:65px; border-right:1px solid black;display:flex;justify-content:center; align-items:center;'>")
                                        a.document.write(x + 1);
                                    a.document.write("</div>");
                                    a.document.write("<div style='height: 28px;width:65px;border-left:1px solid black;display:flex;justify-content:center; align-items:center;'>")
                                        a.document.write(aData[i].NUMBER_OF_BOX);
                                    a.document.write("</div>")
                                a.document.write("</div>")
                                a.document.write("<div style='height: 27px;border-top:1px solid black;border-bottom:0;display:flex;justify-content:center; align-items:center;'>")
                                    a.document.write(aData[i].INVOICE);
                                a.document.write("</div>")
                                a.document.write("<div style='height: 27px;border-top:1px solid black;border-bottom:0;display:flex;justify-content:center; align-items:center;'>")
                                    a.document.write(aData[i].DATE_RECEIVE);
                                a.document.write("</div>")
                                a.document.write("<div style='height: 28px;border-top:1px solid black;display:flex;justify-content:center; align-items:center;'>")
                                    a.document.write("DIP**");
                                a.document.write("</div>")
                            a.document.write("</div>")
                            // a.document.write("<div style='width:130px;border: 1px solid red;'>");

                            // a.document.write("</div>")
                            a.document.write("<div style='width:130px;border: 1px solid black;display:flex;'>");
                                a.document.write("<div style='width:65px;height: 55px;display:flex;margin-top: 2px; margin-left: 2px;'>");
                                    a.document.write(document.getElementById(aData[i].ID).innerHTML);
                                    
                                a.document.write("</div>")
                            a.document.write("</div>")
                        a.document.write("</div>")
                        a.document.write("</div>");
                    }
                }
                
                a.document.write('</body></html>');
                // setTimeout(()=>{
                    
                // }, 2000)
                
                // 
                setTimeout(()=>{
                    a.print();
                    console.log(a)
                    a.close()
                    location.reload(); 
                    Swal.fire({
                    icon: 'success',
                    // title: 'Hooray',
                    text: 'QR code has been printed!',
                    })
                }, 1000)
                })
                
                
            });
        })
    },1000)

let updateQR_printStatus = (dataID)=>{
    return fetch('./api/update_print_status.php', {
    method: 'POST',
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        "data": dataID
    })
    })

    .then(res => {
        
        return res.json()
    })
    .then(result => { 
        return result
    })
}
// bgColor('01')
var bgColor = (month)=>{
    // console.log(month)
    if(month == '01'){
        return {"bgColor":"#0000FF",
                "color": "white"
                }
    }
    else if(month == '02'){
        return {"bgColor":"#8F00FF",
                "color": "white"
                }
    }
    else if(month == '03'){
        return {"bgColor":"#F47F39",
                "color": "black"
                }
    }
    else if(month == '04'){
        return {"bgColor":"#A2B2AC",
                "color": "white"
                }
    }
    else if(month == '05'){
        return {"bgColor":"#4C9A2A",
                "color": "white"
                }
    }
    else if(month == '06'){
        return {"bgColor":"#0D0C12",
                "color": "white"
                }
    }
    else if(month == '07'){
        return {"bgColor":"#FFC0CB",
                "color": "black"
                }
    }
    else if(month == '08'){
        return {"bgColor":"#964B00",
                "color": "white"
                }
    }
    else if(month == '09'){
        return {"bgColor":"#FED758",
                "color": "black"
                }
    }
    else if(month == '10'){
        return {"bgColor":"#7AD7F0",
                "color": "black"
                }
    }
    else if(month == '11'){
        return {"bgColor":"#FFFDFA",
                "color": "black"
                }
    }
    else{
        return {"bgColor":"#880808",
                "color": "white"
                }
    }
// document.getElementById("color-container").textContent = ""
// document.getElementById("goodsCode-container").textContent = document.getElementById('goods_code').textContent
        
}
// window.addEventListener('afterprint', (event) => {
//   alert('After print');
// });
// window.addEventListener('afterprint', (event) => { alert("printed") });
// checkDuplicate();
const checkDuplicate = (nums) => {
    // console.log(nums)
//   nums.sort(); // alters original array
  let ans = []

  for(let i = 0; i< nums.length; i++){
    for(let x = 0; x < nums.length; x++){
        if(nums[i].PO == nums[x].PO && nums[i].GOODS_CODE == nums[x].GOODS_CODE && nums[i].ITEM_CODE == nums[x].ITEM_CODE && nums[i].INVOICE == nums[x].INVOICE && nums[i].DATE_RECEIVE == nums[x].DATE_RECEIVE){
            console.log(nums[i].PO)
        }
    }
  }
}

   

</script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/qrcodejs/1.0.0/qrcode.min.js" integrity="sha512-CNgIRecGo7nphbeZ04Sc13ka07paqdeTu0WR1IM4kNcpmBAUSHSQX0FslNhTDadL4O5SAGapGt4FodqL8My0mA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="./js/print_qr.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>   
<script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
</body>
</html>