<?php

// echo $_GET['result'];
$split = explode(";", $_GET['result']);
// var_dump($split);
// echo ($split[0]);
// $x = 0;
// foreach($split As $data){
//     echo $data."<br>";
    
// }

$subSplit = explode("*", $split[7]);
$partNumberSplit = explode('/', $split[6])


// print_r($split);
// echo $split[7];
// echo $_GET['result'][1];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Result</title>
    <link rel="stylesheet" href="assets/css/main/app.css">
    <link rel="stylesheet" href="assets/css/main/app-dark.css">
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/index.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
</head>
<body style="background-color: #CCCDCF;">
    
    <!-- qr value container -->
    <input type="hidden" id="scannedValue" value="<?php echo $_GET['result']; ?>">

    <div class="container-fluid text-center mx-auto">
        <div class="row logo-container">
            <div class="col text-center" style="display: flex; justify-content:center; align-items:center;">
                <!-- <img class="mt-3" src="./images/matech_logo.png" alt="logo" style="height: 120px; width: 250px;"> -->
                <H1 style="color:white;">Receiving Barcode Scanner</H1>
            </div>
        </div>
        <div class="row mt-5">
            <div class="mt-3 mt-md-0 col-md-4 col-10 barcode_result_container mx-auto order-2 order-md-1" style="background-color: white;">
                <div class="row justify-content-center mt-4">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label for="part_number">PART NUMBER:</label>
                            <input value="<?php echo $partNumberSplit[0]; ?>" type="text" class="form-control text-center" id="part_number" >
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <label for="goods_code">GOODS CODE:</label>
                            <input  type="text" class="form-control text-center" id="goods_code" disabled>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <label for="quantity">QTY RECEIVED:</label>
                            <input value="<?php echo $subSplit[1]; ?>" type="text" class="form-control text-center" id="quantity" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-10 barcode_container mx-auto order-1 order-md-2" id="qr-code" style="background-color: white;">
                <!-- <img src="./images/qrcode.png" alt="qr image" style="width:95%; height:90%"> -->
            </div>
            <!-- <button class="btn btn-primary" onclick="printQR()">Print</button> -->
        </div>
        

        <div class="row mt-3 w-100 mx-auto" style="background-color: white;">
            <div class="row mx-auto mb-3">
                <div id="table-data" class="col-sm-12 mt-4 mb-3" style="overflow-x: scroll;">
                    <table id="data-table" class="table table-bordered mb-3 mt-5" style="min-width: 600px; box-shadow: 0px 3px 10px #888888;">
                        <thead>
                            <tr style="background-color: #3C5393; color:white;">
                                <th class="text-center">ID</th>
                                <th class="text-center">GOODS CODE</th>
                                <th class="text-center">SUPPLIER</th>
                                <th class="text-center">ASSY LINE</th>
                                <th class="text-center">ITEM CODE</th>
                                <th class="text-center">PART NUMBER</th>
                                <th class="text-center">PART NAME</th>
                            </tr>
                        </thead>
                        
                        <tbody id="dataTable"></tbody>
                        
                    </table>
                    
                    <!-- <div class="pagination pagination-primary">
                        <div class="page-item disabled">
                            <div class="page-link" tabindex="-1">Previous</div>
                        </div>
                        <div class="page-item active">
                            <div class="page-link">1</div>
                        </div>
                        <div class="page-item">
                            <div class="page-link">2</div>
                        </div>
                        <div class="page-item">
                            <div class="page-link">3</div>
                        </div>
                        <div class="page-item">
                            <div class="page-link">Next</div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="row mx-auto">
            <center>
                <div id="spinnerBar" class="spinner-border mt-5" style="width: 5rem; height: 5rem;" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
            </center>
            </div>
        </div>
        <div>
            <a href="./scanner_page.php"><img src="./images/qr-code-scan.png" alt="qr icon" class="qr-click"></a>
        </div>
    </div>
    <div id="testContainer"></div>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
   
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="./js/main.js"></script>
    <script src="https://cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>
    <script>
        window.onload = createQRCode();
        window.onload = search();
    </script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
</body>
</html>
