<?php

include 'connection.php';

CREATE TABLE MA_Receiving(
    id INT NOT NULL AUTO_INCREMENT,
    goods_identifier VARCHAR(100), NOT NULL,
    customer_name VARCHAR (50) NOT NULL,
    part_number VARCHAR (50) NOT NULL,
    m/0 VARCHAR (50) NOT NULL,
    trace_number VARCHAR (50) NOT NULL,
    unknown_code VARCHAR (50) NOT NULL,
    delivery_date DATE NOT NULL,
    expiration_date DATE NOT NULL,
    d/c VARCHAR (50) NOT NULL,
    quantity NUMBER NOT NULL,
    PRIMARY KEY (id)
);

// CREATE TABLE [dbo].[username](
//     [id] [INT] NOT NULL AUTO_INCREMENT,
//     [uname] [VARCHAR] (50) NOT NULL
// );