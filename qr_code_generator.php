<div id="printableDiv" class="modal-body w-100">
    
        <div class="row" style="margin-left:10%; border:1px solid red">
            <div class="col-8" style="border:1px solid green;">
                <div class="row">
                    <div class="col">
                        <h5><label id="smt-container">SMT-***</label></h5>
                    </div>
                    <div class="col">
                        <h5><label id="invoice-container"></label></h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h5><label id="itemCode-container"></label></h5>
                    </div>
                    <div class="col">
                        <h5><input id="color-container" style="width:50px;" disabled></h5>   
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h5><label id="goodsCode-container"></label></h5>
                    </div>
                    <div class="col">
                        <h5><label id="boxDetails-container"></label></h5>
                    </div>
                </div>
            </div>
            <div class="col">
                <div id="qr-code" class="mx-auto text-center">
                    
                </div>
            </div>
        </div>